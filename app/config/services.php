<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

date_default_timezone_set('Mexico/General');

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.phtml' => function ($view, $di) use ($config) {

            $volt = new VoltEngine($view, $di);

            $volt->setOptions(array(
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                'compileAlways' => true
            ));

            return $volt;
        },
        '.php' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});


use Phalcon\Logger\Adapter\File as Logger;

class MyDbListener
{
    protected $_logger;

    public function __construct()
    {
        $this->_logger = new Logger("../logs/db.log");
    }

    public function afterQuery($event,  \Phalcon\Db\Adapter\Pdo\Mysql $connection)
    {

        $sql = $connection->getSQLStatement();
        $vars = $connection->getSQLVariables();
        if ($vars) {
            $keys = array();
            $values = array();
            foreach ($vars as $placeHolder=>$var) {
                // fill array of placeholders
                if (is_string($placeHolder)) {
                    $keys[] = '/:'.ltrim($placeHolder, ':').'/';
                } else {
                    $keys[] = '/[?]/';
                }
                // fill array of values
                // It makes sense to use RawValue only in INSERT and UPDATE queries and only as values
                // in all other cases it will be inserted as a quoted string
                if ((strpos($sql, 'INSERT') === 0 || strpos($sql, 'UPDATE') === 0) && $var instanceof \Phalcon\Db\RawValue) {
                    $var = $var->getValue();
                } elseif (is_null($var)) {
                    $var = 'NULL';
                } elseif (is_numeric($var)) {
                    $var = $var;
                } else {
                    $var = '"'.$var.'"';
                }
                $values[] = $var;
            }
            $sql = preg_replace($keys, $values, $sql, 1);
        }

        $this->_logger->log($sql, \Phalcon\Logger::INFO);

    }
}

use Phalcon\Events\Manager as EventsManager;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

$eventsManager = new EventsManager();

// Create a database listener
$dbListener    = new MyDbListener();

// Listen all the database events
$eventsManager->attach('db', $dbListener);
$connection = $di->get('db');


// Assign the eventsManager to the db adapter instance
$connection->setEventsManager($eventsManager);
