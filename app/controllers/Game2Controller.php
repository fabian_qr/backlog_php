<?php

class Game2Controller extends \Phalcon\Mvc\Controller
{

    // https://www.giantbomb.com/api/games/?api_key=adfd7df613277440af75d8edc8fc97d8e6de24e3&format=json&filter=name:super%20mario,platforms:57
    // https://www.giantbomb.com/api/games/?api_key=adfd7df613277440af75d8edc8fc97d8e6de24e3&format=json&filter=name:super%20mario,platforms:9|4
    public function indexAction()
    {

    }

    public function gameStatusAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        if ($idUser = $this->session->get("id")) {
            if ($this->request->isGet()) {
                $this->getGameStatus($idUser);
            } else if ($this->request->isPost()) {
                $this->postGameStatus($idUser);
            } else if ($this->request->isPut()) {
                $this->putGameStatus($idUser);
            } else if ($this->request->isDelete()) {
                $this->deleteGameStatus($idUser);
            } else {
                Util::OUT(false, "It's... I don't know", null, -200);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, -200);
        }
    }

    private function getGameStatus($idUser){
        //Unused
    }

    private function postGameStatus($idUser){
        $idGame = $this->request->getPost("idGame");
        $newStatus = $this->request->getPost("status");


        $gi = new Gameinfo();
        $gi->idGame = $idGame;
        $gi->idUser = $idUser;
        $gi->status = $newStatus;

        $result = $gi->save();

        if ($result) {
            Util::OUT(!$result, "Juego agregado exitosamente", null, 200);
        } else {
            Util::OUT(!$result, "Error al agregar el juego", null, 200);
        }
    }

    private function putGameStatus($idUser){
        $idGame = $this->request->getPut("idGame");
        $newStatus = $this->request->getPut("status");

        if ($game = Gameinfo::findFirst([
            'columns' => '*',
            'conditions' => 'idGame = ?1 AND idUser = ?2',
            'bind' => [
                1 => $idGame,
                2 => $idUser
            ]
        ])
        ) {
            //cambiando...
            $game->status = $newStatus;
            if ($game->save()) {
                Util::OUT(false, "Status cambiado exitosamente", $game, 202);
            } else {
                Util::OUT(true, "Error al cambiar el status del juego", null, 202);
            }

        } else {
            Util::OUT(true, "Error al obtener el juego", null, 202);
        }
    }

    private function deleteGameStatus($idUser){
        $idGame = $this->request->get("idGame");


        if ($game = Gameinfo::findFirst([
            'columns' => '*',
            'conditions' => 'idGame = ?1 AND idUser = ?2',
            'bind' => [
                1 => $idGame,
                2 => $idUser
            ]
        ])
        ) {
            if ($game->delete()) {
                Util::OUT(false, "Juego borrado exitosamente", null, 203);
            } else {
                Util::OUT(true, "Error al borrar el juego", null, 203);
            }
        } else {
            Util::OUT(true, "Error al obtener el juego", null, 203);
        }
    }

    private  $urlConnection = "https://www.giantbomb.com/api";
    private  $keyConnection = "adfd7df613277440af75d8edc8fc97d8e6de24e3";
    private  $limit = 15;


    public function searchGamesByPlatformsAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        $query = $this->request->get("query");
        $page = $this->request->get("page");
        $platforms = $this->request->get("platforms");

        $queryPlats = "";

        foreach($platforms as $plat){
           $queryPlats = $queryPlats.$plat."|";
        }

        $uri = $this->urlConnection."/games/?api_key=".$this->keyConnection."&format=json&filter=name:".$query.",platforms:".$queryPlats;
        $responose =  \Httpful\Request::get($uri)->send();
        $resultsGiant = $responose->body->results;
        $results = $this->fillGames($resultsGiant);

        Util::OUT(false,"Success",$results,101);

    }

    public function searchGamesAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        $query = $this->request->get("query");
        $page = $this->request->get("page");

                    $start_date = new DateTime();
                    $start_mili = microtime();

        $responose =  $this->getGamesFromGiant($query,$page);

                    $end_date = new DateTime();
                    $end_mili = microtime();
                    $since_start = $start_date->diff($end_date);
//                    echo $since_start->days.' days total<br>';
//                    echo $since_start->y.' years<br>';
//                    echo $since_start->m.' months<br>';
//                    echo $since_start->d.' days<br>';
//                    echo $since_start->h.' hours<br>';
//                    echo $since_start->i.' minutes<br>';
                    //echo $since_start->s.' seconds<br>';

        $time_request = $since_start->s+$end_mili-$start_mili." seconds";

        $resultsGiant = $responose->body->results;


        $results = $this->fillGames($resultsGiant);


        Util::OUT(false,"Success",$results,100);
    }

    private function getGamesFromGiant($query, $page){
        $GamesSearchUrl = $this->urlConnection . "/search/?api_key=" .
            $this->keyConnection . "&format=json&resources=game&limit=" . $this->limit;
        $uri = $GamesSearchUrl . "&query=" . $query."&page=".$page;

        return $responose = \Httpful\Request::get($uri)->send();
    }

    private function fillGames($resultsGiant){
        $results = [];

        for($i=0;$i<count($resultsGiant);$i++){
            $result = $resultsGiant[$i];
            array_push($results,$this->fillOneGame($result));
        }
        return $results;
    }

    private function fillOneGame($result){
        $name = $result->name;
        $idGame = $result->id;
        $smallImage = "";
        $bigImage = "";
        if(count($result->image)>0) {
            $smallImage = $result->image->icon_url;
            $bigImage = $result->image->super_url;
        }
        $deck = $result->deck;
        $platforms = $result->platforms;
        $ratings = $result->original_game_rating;
        $launchYear = $result->original_release_date;

        $status = "";
        if ($idUser = $this->session->get("id")) {
            $status = $this->getgameInfoForGameAndUser($idGame,$idUser);
        }
        $avg = $this->getRatingForGame($idGame);

        $plats = [];
        if(count($platforms)>0) {
            foreach ($platforms as $plat) {
                array_push($plats, ["id" => $plat->id, "name" => $plat->name, "abb" => $plat->abbreviation]);
            }
        }

        $rats = [];
        if(count($ratings)>0) {
            foreach ($ratings as $rating) {
                array_push($rats, ["id" => $rating->id, "name" => $rating->name]);
            }
        }

        return [
            "idGame"=>$idGame,
            "name"=>$name,
            "smallImage"=>$smallImage,
            "bigImage"=>$bigImage,
            "deck"=>$deck,
            "platforms"=>$plats,
            "ratings"=>$rats,
            "launchYear"=>$launchYear,
            "status"=>$status,
            "avg"=>$avg
        ];
    }


    public function gamesByStatusAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        if ($idUser = $this->session->get("id")) {
            $status = $this->request->get("status");


            if ($games = Gameinfo::find([
                'columns' => '*',
                'conditions' => 'status = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $status,
                    2 => $idUser
                ]
            ])
            ) {
                $response = [];
                foreach($games as $game){
                    $url = "http://www.giantbomb.com/api/game/".$game->idGame."/?api_key=".$this->keyConnection."&format=json";
                    $responoseJson = \Httpful\Request::get($url)->send();
                    $responseBody = $responoseJson->body->results;
                    $responseGame = $this->fillOneGame($responseBody);
                    array_push($response,$responseGame);
                }

                Util::OUT(false, "Success", $response, 204);

            } else {
                echo "Error";
                Util::OUT(true, "Error al obtener el juego ", ["error_code" => 1], 204);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 200);
        }
    }

    public function gamesByUserAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        if ($id = $this->session->get("id")) {

            $idUser = $this->request->get("idUser");

            $ready = $this->getGamesFromUser($idUser, "NJ");
            $played = $this->getGamesFromUser($idUser, "T");
            $wish = $this->getGamesFromUser($idUser, "WL");
            $inProgress = $this->getGamesFromUser($idUser, "EP");

            $readyGames = $this->fillGamesByStatus($ready);
            $playedGames = $this->fillGamesByStatus($played);
            $wishGames = $this->fillGamesByStatus($wish);
            $inProgressGames = $this->fillGamesByStatus($inProgress);

            $result = [
                "wishlist"=>$wishGames,
                "ready"=>$readyGames,
                "inprogress"=>$inProgressGames,
                "finished"=>$playedGames
            ];
            Util::OUT(false,"ok",$result,904);

        } else {
            Util::OUT(true, "El usuario no está logueado", null, 904);
        }
    }

    private function getGamesFromUser($id, $status){
        return Gameinfo::find([
            'columns' => '*',
            'conditions' => 'idUser = ?1 and status =  ?2',
            'bind' => [
                1 => $id,
                2 => $status
            ]
        ]);
    }

    private function fillGamesByStatus($vec){
        $games = [];
        foreach($vec as $r){
            $url = "http://www.giantbomb.com/api/game/".$r->idGame."/?api_key=".$this->keyConnection."&format=json";
            $responoseJson = \Httpful\Request::get($url)->send();
            $responseBody = $responoseJson->body->results;
            $responseGame = $this->fillOneGame($responseBody);
            array_push($games,$responseGame);
        }

        return $games;
    }

    public function commentAction(){
//        Util::OUT(false,"holi",null,-1);
//        return;

        $this->view->disable();
        header('Content-Type: application/json');

        if ($idUser = $this->session->get("id")) {
            if ($this->request->isGet()) {
                $this->getComment();
            } else if ($this->request->isPost()) {
                $this->postComment($idUser);
            }

        } else {
            Util::OUT(true, "El usuario no está logueado", null, 301);
        }
    }

    private function getComment(){
        $idGame = $this->request->get("idGame");

        $comments = Gamecomments::findByIdGame($idGame);
        if ($comments) {
            $data = [];
            foreach ($comments as $comment) {
                $user = User::findFirst($comment->idUser);
                $name = $user->email;
                if ($user->name != null || $user->name != "") {
                    $name = $user->name;
                }
                array_push($data, ["name" => $name, "comment" => $comment->comment, "rating" => $comment->rating]);
            }
            Util::OUT(false, "Success", $data, 301);
        } else {
            Util::OUT(true, "Error al leer los comentarios", null, 301);
        }
    }

    private function postComment($idUser){
        $idGame = $this->request->getPost("idGame");
        $comment = $this->request->getPost("comment");
        $rating = $this->request->getPost("rating");

        $gc = new Gamecomments();
        $gc->idGame = $idGame;
        $gc->idUser = $idUser;
        $gc->comment = $comment;
        $gc->rating = $rating;

        $result = $gc->save();

        if ($result) {
            Util::OUT(!$result, "Comentario agregado exitosamente", null, 300);
        } else {
            Util::OUT(!$result, "Error al agregar el comentario", null, 300);
        }
    }


    public function platformsAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        $result = Platforms::findByKeep(1);

        if ($result) {
            Util::OUT(!$result, "Success", $result->toArray(), 501);
        } else {
            Util::OUT(!$result, "Error al obtener las plataformas", null, 501);
        }
    }











/*
    // 200
    public function setGameAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {

            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();

            $idGame = $request['idGame'];//$this->request->getPost('idGame');
            $status = $request["status"];//$this->request->getPost('status');

            $gi = new Gameinfo();
            $gi->idGame = $idGame;
            $gi->idUser = $idUser;
            $gi->status = $status;

            $result = $gi->save();

            if ($result) {
                Util::OUT(!$result, "Juego agregado exitosamente", null, 200);
            } else {
                Util::OUT(!$result, "Error al agregar el juego", null, 200);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 200);
        }
    }

    // 201
    public function getGameAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];

            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    2 => $idUser
                ]
            ])
            ) {
                $avg = $this->getRatingForGame($idGame);
                $game->avg = $avg;
                Util::OUT(false, "Success", $game, 201);
            } else {
                $avg = $this->getRatingForGame($idGame);
                Util::OUT(true, "Error al obtener el juego ", ["avg" => $avg], 201);
            }
        } else {

            Util::OUT(true, "El usuario no está logueado", null, 201);
        }
    }

    // 202
    public function changeGameAction()
    {
        $this->view->disable();
        //.-.-
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();

            $idGame = $request["idGame"];// $this->request->getPost('idGame');
            $status = $request["status"];//$this->request->getPost('status');


            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    //.-.-
                    2 => $idUser
                    //2 => 3
                ]
            ])
            ) {
                //cambiando...
                $game->status = $status;
                if ($game->save()) {
                    Util::OUT(false, "Status cambiado exitosamente", $game, 202);
                } else {
                    Util::OUT(true, "Error al cambiar el status del juego", null, 202);
                }

            } else {
                //juego no existe, creando...


                $gi = new Gameinfo();
                $gi->idGame = $idGame;
                //.-.-
                $gi->idUser = $idUser;
                $gi->status = $status;

                $result = $gi->save();

                if ($result) {
                    Util::OUT(!$result, "Juego agregado exitosamente", $gi, 200);
                } else {
                    Util::OUT(!$result, "Error al agregar el juego", null, 200);
                }

                //Util::OUT(true, "Error al obtener el juego ", null, 202);
            }
            //.-.-
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 202);
        }
    }

    // 203
    public function deleteGameAction()
    {
        $this->view->disable();
        //.-.-
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];//$this->request->getPost('idGame');

            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    //.-.-
                    2 => $idUser
                    //2 => 3
                ]
            ])
            ) {
                if ($game->delete()) {
                    Util::OUT(false, "Juego borrado exitosamente", null, 203);
                } else {
                    Util::OUT(true, "Error al borrar el juego", null, 203);
                }
            } else {
                Util::OUT(true, "Error al obtener el juego", null, 203);
            }
            //.-.-
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 203);
        }
    }

    // 204
    public function getGameByAction()
    {
//        echo json_encode("Holi");
//        return;

        $this->view->disable();

        //----
//        echo var_dump("Get Game By");
//        return;

        //.-.-
        if ($this->request->isPost()) {
            if ($idUser = $this->session->get("id")) {
                parse_str(file_get_contents("php://input"), $_POST);


                $request = $this->decodifyFromJson();
                $status = $request["status"];


                if ($game = Gameinfo::find([
                    'columns' => '*',
                    'conditions' => 'status = ?1 AND idUser = ?2',
                    'bind' => [
                        1 => $status,
                        //.-.-
                        2 => $idUser
                        //2 => 3
                    ]
                ])
                ) {
                    Util::OUT(false, "Success", $game->toArray(), 204);
                } else {
                    echo "Error";
                    Util::OUT(true, "Error al obtener el juego ", ["error_code" => 1], 204);
                }
                //.-.-
            } else {
                Util::OUT(true, "El usuario no está logueado", null, 204);
            }
        } else {
            Util::OUT(true, "Se necesita método post", null, 204);
        }
    }

    // 300
    public function setCommentAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {

            parse_str(file_get_contents("php://input"), $_POST);
            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];//$this->request->getPost('idGame');
            $comment = $request["comment"];//$this->request->getPost('comment');
            $rating = $request["rating"];//$this->request->getPost('rating');

            $gc = new Gamecomments();
            $gc->idGame = $idGame;
            $gc->idUser = $idUser;
            $gc->comment = $comment;
            $gc->rating = $rating;

            $result = $gc->save();

            if ($result) {
                Util::OUT(!$result, "Comentario agregado exitosamente", null, 300);
            } else {
                Util::OUT(!$result, "Error al agregar el comentario", null, 300);
            }
        } else {

            Util::OUT(true, "El usuario no está logueado", null, 300);
        }

    }

    // 301
    public function getCommentsAction()
    {
        $this->view->disable();

        $request = $this->decodifyFromJson();
        $id = $request["idGame"];

        $comments = Gamecomments::findByIdGame($id);
        if ($comments) {
            $data = [];
            foreach ($comments as $comment) {
                $user = User::findFirst($comment->idUser);

                $name = $user->email;
                if ($user->name != null || $user->name != "") {
                    $name = $user->name;
                }

                array_push($data, ["name" => $name, "comment" => $comment->comment, "rating" => $comment->rating]);
            }

            //$data = $comments->toArray();

            Util::OUT(false, "Success", $data, 301);
        } else {
            Util::OUT(true, "Error al leer los comentarios", null, 301);
        }
    }
*/

    /*
    public function getStatusesAction()
    {
        $this->view->disable();

        if ($idUser = $this->session->get("id")) {
            $request = $this->decodifyFromJson();
            $games = $request["games"];
            $responseResult = [];
            foreach ($games as $game) {
                $idGame = $game["id"];
                $rating = $this->getRatingForGame($idGame);
                $gameInfo = $this->getgameInfoForGameAndUser($idGame, $idUser);

                array_push($responseResult, ["avg" => $rating, "status" => $gameInfo]);
            }
            Util::OUT(false, "Success", $responseResult, 302);
        } else {
            $request = $this->decodifyFromJson();
            $games = $request["games"];
            $responseResult = [];
            foreach ($games as $game) {
                $idGame = $game["id"];

                $rating = $this->getRatingForGame($idGame);
                array_push($responseResult, ["avg" => $rating, "status" => ""]);
            }
            Util::OUT(true, "El usuario no está logueado", $responseResult, 300);
        }
    }
*/
    public function getRatingForGame($idGame)
    {
        $results = Gamecomments::findByIdGame($idGame);

        $avg = 0;
        foreach ($results as $result) {
            $avg += $result->rating;
        }

        $avg = $avg == 0 ? 0 : $avg / $results->count();

        return $avg;
    }

    public function getgameInfoForGameAndUser($idGame, $idUser)
    {

        if ($game = Gameinfo::findFirst([
            'columns' => '*',
            'conditions' => 'idGame = ?1 AND idUser = ?2',
            'bind' => [
                1 => $idGame,
                //.-.-
                2 => $idUser
                //2 => 3
            ]
        ])
        ) {
            return $game->status;
        } else {
            return "AA";
        }
    }

    /*
    private function decodifyFromJson()
    {
        $handle = fopen("php://input", "rb");
        $raw_post_data = '';
        while (!feof($handle)) {
            $raw_post_data .= fread($handle, 8192);
        }
        fclose($handle);

        $request = json_decode($raw_post_data, true);

        return $request;
    }
    */
}

