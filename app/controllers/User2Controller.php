<?php

class User2Controller extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }


    public function changeNameAction()
    {
        $this->view->disable();

        if ($id = $this->session->get("id")) {

            //Old method
            /*
            $request = $this->decodifyFromJson();
            $email = $request["email"];
            $name = $request["name"];
            // */

//*
            $email = $this->request->getPut("email");
            $name = $this->request->getPut("name");
// */

            if ($email == ""){
                Util::OUT(true,"Correo no puede contener campos vacíos",null,100);
                return;
            }
            $user = User::findFirst($id);

            if ($user) {
                $user->email = $email;
                $user->name = $name;
                if ($user->save()) {
                    Util::OUT(false, "Guardado exitoso", ["email"=>$user->email,"id"=>$user->id,"name"=>$user->name], 123);
                } else {
                    Util::OUT(true, "Error al guardar cambios 1", null, 456);
                }
            } else {
                Util::OUT(true, "El usuario no existe", null, 789);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 111);
        }

    }

    public function changeNameAndPassAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {

            /*
            $request = $this->decodifyFromJson();
            $email = $request["email"];
            $name = $request["name"];
            $pass = $request["pass"];
            $oldPass = $request["oldPass"];
            // */

//*
            $email = $this->request->getPut("email");
            $name = $this->request->getPut("name");
            $pass = $this->request->getPut("pass");
            $oldPass = $this->request->getPut("oldPass");

            if ($email == "" || $pass == ""){
                Util::OUT(true,"Correo / contraseña no pueden contener campos vacíos",null,300);
                return;
            }
// */
            $user = User::findFirst($id);

            if ($user) {
                if (password_verify($oldPass, $user->pass)) {
                    $user->email = $email;
                    $user->name = $name;
                    $user->pass = password_hash($pass, PASSWORD_DEFAULT);

                    $result = $user->save();

                    if ($result) {
                        Util::OUT(false, "Guardado exitoso", ["email"=>$user->email,"id"=>$user->id,"name"=>$user->name], 300);
                    } else {
                        Util::OUT(true, "Error al guardar cambios", null, 300);
                    }
                } else {
                    Util::OUT(true, "El password no coincide", null, 300);
                }
            } else {
                Util::OUT(true, "El usuario no existe", null, 300);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 300);
        }
    }

    public function findAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {
/*
            $request = $this->decodifyFromJson();
            $word = $request["query"];
//  */

//*
            $word = $this->request->get("query");
// */
            $users = User::find([
                'columns' => 'id, email, name',
                'conditions' => "name LIKE ?1 or email LIKE ?2  ",
                'bind' => [
                    1 => "%" . $word . "%",
                    2 => "%" . $word . "%"
                ]
            ]);

            $resulset = [];

            foreach ($users as $user) {

                $follow = Follow::find([
                    'columns' => '*',
                    'conditions' => 'user = ?1 and followingUser = ?2',
                    'bind' => [
                        1 => $id,
                        2 => $user->id
                    ]
                ]);

                if ($follow->count() > 0) {
                    array_push($resulset, ["id" => $user->id, "email" => $user->email, "name" => $user->name, "following" => true]);
                } else {
                    array_push($resulset, ["id" => $user->id, "email" => $user->email, "name" => $user->name, "following" => false]);
                }
            }

            if ($users) {
                Util::OUT(false, "Success", $resulset, 300);
            } else {
                Util::OUT(true, "Error al consultar", null, 300);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 300);
        }
    }

    public function followAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {
/*
            $request = $this->decodifyFromJson();
            $followingUser = $request["idFollowing"];
// */

//*
            $followingUser = $this->request->getPost("idFollowing");
// */
            if($resultRepeated = Follow::findFirst([
                'columns' => '*',
                'conditions' => 'user = ?1 and followingUser =  ?2',
                'bind' => [
                    1 => $id,
                    2 => $followingUser
                ]
            ])){

                Util::OUT(true, "Ya se está siguiendo al usuario", null, 901);
                return;
            }

            $newFollow = new Follow();
            $newFollow->user = $id;
            $newFollow->followingUser = $followingUser;

            if ($newFollow->create()) {
                Util::OUT(false, "Éxito al seguir", $newFollow, 901);
            } else {
                Util::OUT(true, "Error al seguir", null, 901);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 901);
        }
    }

    public function unFollowAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {
/*
            $request = $this->decodifyFromJson();
            $followingUser = $request["idFollowing"];
// */

//*
            $followingUser = $this->request->get("idFollowing");
// */
            $deleteFollow = Follow::findFirst([
                'columns' => '*',
                'conditions' => 'user = ?1 and followingUser = ?2',
                'bind' => [
                    1 => $id,
                    2 => $followingUser
                ]
            ]);

            if ($deleteFollow) {
                if ($deleteFollow->delete()) {
                    Util::OUT(false, "Éxito al dejar de seguir", null, 902);
                } else {
                    Util::OUT(true, "Error al dejar de seguir", null, 902);
                }
            } else {
                Util::OUT(true, "Error al obtener el usuario", null, 902);
            }

        } else {
            Util::OUT(true, "El usuario no está logueado", null, 902);
        }
    }

    public function getFollowingAndFollowersAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {
            $followingResult = [];
            $followersResult = [];

            $following = Follow::findByUser($id);
            $followers = Follow::findByFollowingUser($id);

            //Fill data for following
            foreach ($following as $f1) {
                $user = User::findFirst($f1->followingUser);
                array_push($followingResult, ["id" => $user->id, "email" => $user->email, "name" => $user->name, "following" => true]);
            }

            //Fill data for followers
            foreach ($followers as $f2) {
                $user = User::findFirst($f2->user);

                $follow = Follow::find([
                    'columns' => '*',
                    'conditions' => 'user = ?1 and followingUser = ?2',
                    'bind' => [
                        1 => $id,
                        2 => $user->id
                    ]
                ]);

                $followingB = false;

                if ($follow->count() > 0) {
                    $followingB = true;
                }

                array_push($followersResult, ["id" => $user->id, "email" => $user->email, "name" => $user->name, "following" => $followingB]);
            }

            if ($following && $followers) {
                Util::OUT(false, "ok", ["following" => $followingResult, "followers" => $followersResult], 903);
            } else {
                Util::OUT(true, "Error al obtener seguidores y siguiendo", null, 903);
            }

        } else {
            Util::OUT(true, "El usuario no está logueado", null, 903);
        }
    }

    public function userGamesAction()
    {
        $this->view->disable();
        if ($id = $this->session->get("id")) {
//*
            $request = $this->decodifyFromJson();
            $idUser = $request["id"];
// */

/*
            $idUser = $this->request->get("id");
// */
            $ready = $this->getGamesFromUser($idUser, "NJ");
            $played = $this->getGamesFromUser($idUser, "T");
            $wish = $this->getGamesFromUser($idUser, "WL");
            $inProgress = $this->getGamesFromUser($idUser, "EP");

            $result = [
                "wishlist"=>$wish->toArray(),
                "ready"=>$ready->toArray(),
                "inprogress"=>$inProgress->toArray(),
                "finished"=>$played->toArray()
            ];
            Util::OUT(false,"ok",$result,904);

        } else {
            Util::OUT(true, "El usuario no está logueado", null, 904);
        }
    }

    private function getGamesFromUser($id, $status){
        return Gameinfo::find([
            'columns' => '*',
            'conditions' => 'idUser = ?1 and status =  ?2',
            'bind' => [
                1 => $id,
                2 => $status
            ]
        ]);
    }

    private function decodifyFromJson()
    {
        $handle = fopen("php://input", "rb");
        $raw_post_data = '';
        while (!feof($handle)) {
            $raw_post_data .= fread($handle, 8192);
        }
        fclose($handle);

        $request = json_decode($raw_post_data, true);

        return $request;
    }

}

