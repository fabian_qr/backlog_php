<?php

class GameController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }

    /**
     *
     */


    private static $urlConnection = "https://www.giantbomb.com/api";
    private static $keyConnection = "adfd7df613277440af75d8edc8fc97d8e6de24e3";
    private static $limit = 10;


    //1
    public function getGamesAction($gameName)
    {

        $GamesSearchUrl = GameController::$urlConnection . "/search/?api_key=" . GameController::$keyConnection . "&format=json&resources=game&limit=" . GameController::$limit;
        $uri = $GamesSearchUrl . "&query=" . $gameName;

    }


    // 200
    public function setGameAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {

            //Util::OUT(false, "asd", $idUser);

            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();

            $idGame = $request['idGame'];//$this->request->getPost('idGame');
            $status = $request["status"];//$this->request->getPost('status');

            $gi = new Gameinfo();
            $gi->idGame = $idGame;
            $gi->idUser = $idUser;
            $gi->status = $status;

            $result = $gi->save();

            if ($result) {
                Util::OUT(!$result, "Juego agregado exitosamente", null, 200);
            } else {
                Util::OUT(!$result, "Error al agregar el juego", null, 200);
            }
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 200);
        }
    }

    // 201
    public function getGameAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];

            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    //.-.-
                    2 => $idUser
                    //2 => 3
                ]
            ])
            ) {
                $avg = $this->getRatingForGame($idGame);
                $game->avg = $avg;
                Util::OUT(false, "Success", $game, 201);
            } else {
                $avg = $this->getRatingForGame($idGame);
                Util::OUT(true, "Error al obtener el juego ", ["avg" => $avg], 201);
            }
        } else {

            Util::OUT(true, "El usuario no está logueado", null, 201);
        }
    }

    // 202
    public function changeGameAction()
    {
        $this->view->disable();
        //.-.-
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();

            $idGame = $request["idGame"];// $this->request->getPost('idGame');
            $status = $request["status"];//$this->request->getPost('status');


            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    //.-.-
                    2 => $idUser
                    //2 => 3
                ]
            ])
            ) {
                //cambiando...
                $game->status = $status;
                if ($game->save()) {
                    Util::OUT(false, "Status cambiado exitosamente", $game, 202);
                } else {
                    Util::OUT(true, "Error al cambiar el status del juego", null, 202);
                }

            } else {
                //juego no existe, creando...


                $gi = new Gameinfo();
                $gi->idGame = $idGame;
                //.-.-
                $gi->idUser = $idUser;
                $gi->status = $status;

                $result = $gi->save();

                if ($result) {
                    Util::OUT(!$result, "Juego agregado exitosamente", $gi, 200);
                } else {
                    Util::OUT(!$result, "Error al agregar el juego", null, 200);
                }

                //Util::OUT(true, "Error al obtener el juego ", null, 202);
            }
            //.-.-
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 202);
        }
    }

    // 203
    public function deleteGameAction()
    {
        $this->view->disable();
        //.-.-
        if ($idUser = $this->session->get("id")) {
            parse_str(file_get_contents("php://input"), $_POST);

            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];//$this->request->getPost('idGame');

            if ($game = Gameinfo::findFirst([
                'columns' => '*',
                'conditions' => 'idGame = ?1 AND idUser = ?2',
                'bind' => [
                    1 => $idGame,
                    //.-.-
                    2 => $idUser
                    //2 => 3
                ]
            ])
            ) {
                if ($game->delete()) {
                    Util::OUT(false, "Juego borrado exitosamente", null, 203);
                } else {
                    Util::OUT(true, "Error al borrar el juego", null, 203);
                }
            } else {
                Util::OUT(true, "Error al obtener el juego", null, 203);
            }
            //.-.-
        } else {
            Util::OUT(true, "El usuario no está logueado", null, 203);
        }
    }

    // 204
    public function getGameByAction()
    {
//        echo json_encode("Holi");
//        return;

        $this->view->disable();

        //----
//        echo var_dump("Get Game By");
//        return;

        //.-.-
        if ($this->request->isPost()) {
            if ($idUser = $this->session->get("id")) {
                parse_str(file_get_contents("php://input"), $_POST);


                $request = $this->decodifyFromJson();
                $status = $request["status"];


                if ($game = Gameinfo::find([
                    'columns' => '*',
                    'conditions' => 'status = ?1 AND idUser = ?2',
                    'bind' => [
                        1 => $status,
                        //.-.-
                        2 => $idUser
                        //2 => 3
                    ]
                ])
                ) {
                    Util::OUT(false, "Success", $game->toArray(), 204);
                } else {
                    echo "Error";
                    Util::OUT(true, "Error al obtener el juego ", ["error_code" => 1], 204);
                }
                //.-.-
            } else {
                Util::OUT(true, "El usuario no está logueado", null, 204);
            }
        } else {
            Util::OUT(true, "Se necesita método post", null, 204);
        }
    }

    // 300
    public function setCommentAction()
    {
        $this->view->disable();
        if ($idUser = $this->session->get("id")) {

            parse_str(file_get_contents("php://input"), $_POST);
            $request = $this->decodifyFromJson();
            $idGame = $request["idGame"];//$this->request->getPost('idGame');
            $comment = $request["comment"];//$this->request->getPost('comment');
            $rating = $request["rating"];//$this->request->getPost('rating');

            $gc = new Gamecomments();
            $gc->idGame = $idGame;
            $gc->idUser = $idUser;
            $gc->comment = $comment;
            $gc->rating = $rating;

            $result = $gc->save();

            if ($result) {
                Util::OUT(!$result, "Comentario agregado exitosamente", null, 300);
            } else {
                Util::OUT(!$result, "Error al agregar el comentario", null, 300);
            }
        } else {

            Util::OUT(true, "El usuario no está logueado", null, 300);
        }

    }

    // 301
    public function getCommentsAction()
    {
        $this->view->disable();

        $request = $this->decodifyFromJson();
        $id = $request["idGame"];

        //$id = 1122;

        $comments = Gamecomments::findByIdGame($id);
        if ($comments) {
            $data = [];
            foreach ($comments as $comment) {
                $user = User::findFirst($comment->idUser);

                $name = $user->email;
                if ($user->name != null || $user->name != "") {
                    $name = $user->name;
                }

                array_push($data, ["name" => $name, "comment" => $comment->comment, "rating" => $comment->rating]);
            }

            //$data = $comments->toArray();

            Util::OUT(false, "Success", $data, 301);
        } else {
            Util::OUT(true, "Error al leer los comentarios", null, 301);
        }
    }

    public function getStatusesAction()
    {
        $this->view->disable();

        if ($idUser = $this->session->get("id")) {
            $request = $this->decodifyFromJson();
            $games = $request["games"];
            $responseResult = [];
            foreach ($games as $game) {
                $idGame = $game["id"];

                $rating = $this->getRatingForGame($idGame);
                $gameInfo = $this->getgameInfoForGameAndUser($idGame, $idUser);

                array_push($responseResult, ["avg" => $rating, "status" => $gameInfo]);
            }
            Util::OUT(false, "Success", $responseResult, 302);
        } else {
            $request = $this->decodifyFromJson();
            $games = $request["games"];
            $responseResult = [];
            foreach ($games as $game) {
                $idGame = $game["id"];

                $rating = $this->getRatingForGame($idGame);
                array_push($responseResult, ["avg" => $rating, "status" => ""]);
            }
            Util::OUT(true, "El usuario no está logueado", $responseResult, 302);
        }
    }

    public function getPlatformsAction(){
        $this->view->disable();


    }

    public function getRatingForGame($idGame)
    {
        $results = Gamecomments::findByIdGame($idGame);

        $avg = 0;
        foreach ($results as $result) {
            $avg += $result->rating;
        }

        $avg = $avg == 0 ? 0 : $avg / $results->count();

        return $avg;
    }

    public function getgameInfoForGameAndUser($idGame, $idUser)
    {

        if ($game = Gameinfo::findFirst([
            'columns' => '*',
            'conditions' => 'idGame = ?1 AND idUser = ?2',
            'bind' => [
                1 => $idGame,
                //.-.-
                2 => $idUser
                //2 => 3
            ]
        ])
        ) {
            return $game->status;
        } else {
            return "AA";
        }
    }

    private function decodifyFromJson()
    {
        $handle = fopen("php://input", "rb");
        $raw_post_data = '';
        while (!feof($handle)) {
            $raw_post_data .= fread($handle, 8192);
        }
        fclose($handle);

        $request = json_decode($raw_post_data, true);

        return $request;
    }


    public function setGameDBAction()
    {

    }
}

