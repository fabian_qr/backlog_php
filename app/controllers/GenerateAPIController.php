<?php

class GenerateAPIController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }

    public function setGameAction($id,$launch){
        $this->view->disable();
        $g = new Games();

        parse_str(file_get_contents("php://input"), $_POST);

        $small = $this->request->get("small");
        $big = $this->request->get("big");
        $name = $this->request->get("name");
        $deck = $this->request->get("deck");


        $g->id = $id;
        $g->name = $name;
        $g->smallImage = $small;
        $g->bigImage = $big;
        $g->deck = $deck;
        $g->launchYear = $launch;

        $g->save();
    }

    public function setRatingAction($id, $name){
        $this->view->disable();
        $rat = new Rating();

        $rat->id = $id;
        $rat->name = $name;

        $rat->save();
    }

    public function setPlatformAction($id, $abb,$name){
        $this->view->disable();
        $plat = new Platforms();

        $plat->id = $id;
        $plat->abbreviation = $abb;
        $plat->name = $name;

        $plat->save();
    }

    public function setGameRatingAction($idG,$idR){
        $this->view->disable();
        $gr = new GameRating();

        $gr->idGame = $idG;
        $gr->idRating = $idR;

        $gr->save();
    }

    public function setGamePlatformAction($idG,$idP){
        $this->view->disable();
        $gp = new GamePlatform();
        $gp->idGame = $idG;
        $gp->idPlatform = $idP;

        $gp->save();
    }

}

