<?php

class SessionController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
    // 100
    public function registerAction()
    {
        $this->view->disable();

        //if the user is in a current session, return an error
        if ($this->session->get("id")) {
            Util::OUT(true, "El usuario ya está logueado", null,100);
        } else {
            parse_str(file_get_contents("php://input"), $_POST);
            //if isn't a POST method, return an error
            if ($this->request->isPost()) {



                $request = $this->decodifyFromJson();
                $email = $request["email"];// $this->request->getPost('email');
                $pass = $request["pass"];//$this->request->getPost('pass');


//                $email = $this->request->getpost("email");
//                $pass = $this->request->getPost("pass");

                if ($email == "" || $pass == ""){
                    Util::OUT(true,"Correo / contraseña no pueden contener campos vacíos",null,100);
                    return;
                }

                //if the user exists, there's an error
                if (User::findFirstByEmail($email)) {
                    Util::OUT(true, "El correo ya está registrado", null,100);
                } else {
                    $newUser = new User();
                    $newUser->email = $email;
                    $newUser->pass = password_hash($pass, PASSWORD_DEFAULT);
                    $response = $newUser->create();
                    //var_dump($newUser->getMessages());

                    if ($response) {
                        Util::OUT(!$response, "Usuario creado exitosamente", null,100);
                    } else {
                        Util::OUT(!$response, "Error al crear el usuario", null,100);
                    }
                }
            } else {
                Util::OUT(true, "Se requiere método POST", null,100);
            }
        }
    }
    // 101
    public function loginAction()
    {
        $this->view->disable();

        if ($this->session->get("id")) {
            Util::OUT(true, "El usuario ya está logueado", null,101);
        } else {
            //parse_str(file_get_contents("php://input"), $_POST);
            //if isn't a POST method, return an error
            if ($this->request->isPost()) {

                //old method, data are json

                $request = $this->decodifyFromJson();
                $email = $request["email"];// $this->request->getPost('email');
                $pass = $request["pass"];//$this->request->getPost('pass');


//                $email = $this->request->getPost("email");
//                $pass = $this->request->getPost("pass");


                if ($user = User::findFirstByEmail($email)) {
                    if ($this->verifyUser($user, $pass)) {
                        $this->session->set("id", $user->id);
                        $reducedUser = array(
                            "id"=>$user->id,
                            "email"=>$user->email,
                            "name"=>$user->name
                            );
//                        $reducedUser->id = $user->id;
//                        $reducedUser->email = $user->email;
//                        $reducedUser->name = $user->name;

                        $data = ['user'=>$reducedUser];

                        Util::OUT(false, "Login exitoso",$data,101);
                    } else {
                        Util::OUT(true, "Usuario/contraseña incorrecta 1", null,101);
                    }
                } else {
                    Util::OUT(true, "Usuario/contraseña incorrecta 2", ["email"=>$email, "pass"=>$pass],101);
                }
            } else {
                Util::OUT(true, "Se requiere método POST", null,101);
            }
        }
    }
    // 102
    public function logoutAction()
    {
        $this->view->disable();

        if ($this->session->get("id")) {
            $this->session->remove("id");
            Util::OUT(false, "Logout exitoso", null,102);
        } else {
            Util::OUT(true, "El usuario no está logueado", null,102);
        }
    }
    //103
    public function isLoggedAction(){
        $this->view->disable();

        if ($id = $this->session->get("id")) {
            Util::OUT(false, "Usuario Logueado", User::findFirst($id),103);
        } else {
            Util::OUT(true,"Usuario no logueado",null,103);
        }
    }

    private function verifyUser($user, $pass)
    {
        if (password_verify($pass, $user->pass)) {
            return true;
        }
        return false;
    }

    private function decodifyFromJson()
    {
        $handle = fopen("php://input", "rb");
        $raw_post_data = '';
        while (!feof($handle)) {
            $raw_post_data .= fread($handle, 8192);
        }
        fclose($handle);

        $request = json_decode($raw_post_data, true);

        return $request;
    }
}

