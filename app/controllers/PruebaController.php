<?php

class PruebaController extends \Phalcon\Mvc\Controller
{

    private static $urlConnection = "https://www.giantbomb.com/api";
    private static $keyConnection = "adfd7df613277440af75d8edc8fc97d8e6de24e3";
    private static $limit = 10;

    public function indexAction()
    {
        header('Content-Type: application/json');

        $this->view->disable();

        //$responose = \Httpful\Request::get("https://www.giantbomb.com/api/game/1000/?api_key=adfd7df613277440af75d8edc8fc97d8e6de24e3&format=json")->send();

//        echo json_encode($responose);

        //var_dump($responose->body->results->deck);


        $GamesSearchUrl = PruebaController::$urlConnection . "/search/?api_key=" . PruebaController::$keyConnection . "&format=json&resources=game&limit=" . PruebaController::$limit;
        $uri = $GamesSearchUrl . "&query=" . "super mario";

        $responose = \Httpful\Request::get($uri)->send();

        $results = $responose->body->results;

        $plats = [];

        for($i=0;$i<count($results);$i++){

            $result = $results[$i];

            $name = $result->name;
            $idGame = $result->id;
            $smallImage = $result->image->icon_url;
            $bigImage = $result->image->super_url;
            $deck = $result->deck;
            $platforms = $result->platforms;
            $rating = $result->original_game_rating;
            $launchYear = $result->original_release_date;

            $status = "";
            $avg = "";

//            var_dump("\nNAME: ",$name);
//            var_dump("\nIDGAME: ",$idGame);
//            var_dump("SMALL: ",$smallImage);
//            var_dump("BIG",$bigImage);
//            var_dump("DECK: ",$deck);
//            var_dump("PLATFORMS",$platforms);


            array_push($plats,$platforms);
//            var_dump("RATING",$rating);
//            var_dump("LAUNCH YEAR",$launchYear);

        }
        echo json_encode(["Platforms"=>$plats]);
        //var_dump("RESPONSE: ",$responose);
    }

    // 100
    public function tipoConsultaAction(){
        $this->view->disable();
        header('Content-Type: application/json');

        if ($this->request->isGet()){
            $this->getGame();
        }
        else if ($this->request->isPost()){
            $this->postGame();
        }
        else if($this->request->isPut()){
            $this->putGame();
        }
        else if($this->request->isDelete()){
            $this->deleteGame();
        }
        else{
            Util::OUT(false,"Is... I don't know",null,-100);
        }
    }

    private function getGame(){
        $idGame = $this->request->get("idGame");
        Util::OUT(false,"Is Get",["idGame"=>$idGame],100);
    }
    private function postGame(){
        $idGame = $this->request->getPost("idGame");
        $name =$this->request->getPost("name");
        $deck = $this->request->getPost("deck");
        Util::OUT(false,"Is Post",
            ["idGame"=>$idGame,"name"=>$name,"deck"=>$deck]
            ,101);
    }
    private function putGame(){
        $idGame = $this->request->getPut("idGame");
        $name =$this->request->getPut("name");
        Util::OUT(false,"Is Put",["idGame"=>$idGame,"name"=>$name],102);
    }
    private function deleteGame(){
        $idGame = $this->request->get("idGame");
        Util::OUT(false,"Is Delete",["idGame"=>$idGame],103);
    }



    private function decodifyFromJson()
    {
        $handle = fopen("php://input", "rb");
        $raw_post_data = '';
        while (!feof($handle)) {
            $raw_post_data .= fread($handle, 8192);
        }
        fclose($handle);

        $request = json_decode($raw_post_data, true);

        return $request;
    }


}

