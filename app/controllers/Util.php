<?php

/**
 * Created by PhpStorm.
 * User: fabian
 * Date: 17/11/15
 * Time: 8:09 AM
 */
class Util
{

    public static function OUT($error,$message,$data,$code){
        $response = ["error"=>$error, "mensaje"=>$message,"data"=>$data,"code"=>$code];
        echo json_encode($response);
    }

    public static function GET_DATE(){
        return date("Y-n-j H:i:s");
    }

    public static function setModelErrorMessage($model){
        $mess = "";
        foreach($model->getMessages() as $message){


            $mess += $message->getMessage()/*.$message->getField().$message->getType()*/.' \n ';

        }

        return $mess;
    }



}