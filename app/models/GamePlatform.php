<?php

class GamePlatform extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idGame;

    /**
     *
     * @var integer
     */
    public $idPlatform;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('idGame', 'Games', 'id', array('alias' => 'Games'));
        $this->belongsTo('idPlatform', 'Platforms', 'id', array('alias' => 'Platforms'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'game_platform';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return GamePlatform[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return GamePlatform
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
