<?php

class Games extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $smallImage;

    /**
     *
     * @var string
     */
    public $bigImage;

    /**
     *
     * @var string
     */
    public $deck;

    /**
     *
     * @var string
     */
    public $launchYear;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'GamePlatform', 'idGame', array('alias' => 'GamePlatform'));
        $this->hasMany('id', 'GameRating', 'idGame', array('alias' => 'GameRating'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'games';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Games[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Games
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
