<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class GamePlatformMigration_102
 */
class GamePlatformMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('game_platform', array(
                'columns' => array(
                    new Column(
                        'idGame',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        )
                    ),
                    new Column(
                        'idPlatform',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'idGame'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('idGame', array('idGame')),
                    new Index('idPlatform', array('idPlatform'))
                ),
                'references' => array(
                    new Reference(
                        'game_platform_ibfk_1',
                        array(
                            'referencedSchema' => 'backlog',
                            'referencedTable' => 'games',
                            'columns' => array('idGame'),
                            'referencedColumns' => array('id')
                        )
                    ),
                    new Reference(
                        'game_platform_ibfk_2',
                        array(
                            'referencedSchema' => 'backlog',
                            'referencedTable' => 'platforms',
                            'columns' => array('idPlatform'),
                            'referencedColumns' => array('id')
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'latin1_swedish_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
